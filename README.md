## Tarea semana 1 "Red de bicicletas" node.js backend - coursera

### Inicio

Este repositorio es producto del conocimiento adquirido con el contenido de la semana 1 del presente curso.

### Contenido

* Servior Node.js con plantilla 'pug' de express
* Visualización de puntos por coordenadas en mapa
* Método MVC para endpoints, API
* Guardado en memoria de registros de bicicletas

### Como correr el ejemplo

* Clonar el repositorio:

```
git clone https://nandocl@bitbucket.org/nandocl/coursera-ejemplo-1.git
```

* Instalar las dependencias:

```
npm install
```

* Ejecutar:

```
npm start
```

* Link Heroku:

https://red-bicicletas-nandocl.herokuapp.com