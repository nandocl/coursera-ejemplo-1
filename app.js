require('newrelic')
require('dotenv').config()
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
require('./config/passport');
var session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasRouterApi = require('./routes/api/bicicletasRApi');
var userRApi = require('./routes/api/usuarios')
const authAPIRouter = require('./routes/api/auth')

const tokenRouter = require('./routes/token')
const usuariosRouter = require('./routes/usuarios')

//Modelo usuario
const Usuario = require('./modelos/usuario')
const Token = require('./modelos/token')

var app = express();

let store;
if (process.env.NODE_ENV === 'develpment'){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  })
}

app.set('secretKey', 'jwt_pwd_!!223344')

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'redBicisNandocl'
}));

//mongoDb config
const mongoose = require('mongoose');

// const mongoUrl = 'mongodb://localhost/red_bicicletas'
// const mongoUrl = 'mongodb+srv://admin:hhidkfahh@red-bicicletas-nandocl-lfsui.mongodb.net/red-bicicletas-nandocl?retryWrites=true&w=majority'
const mongoUrl = process.env.MONGO_URI
mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true})
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Mongodb connection error'))

// Add the line below, which you're missing:
// require('./config/passport')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res) {
  res.render('session/login')
})

app.post('/login', function(req, res, next) {  
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err)
    if(!usuario) return res.render('session/login', {info})
    req.logIn(usuario, function(err) {
      if(err) return next(err)
      res.redirect('/')
    })
  })(req, res, next)
})

app.get('/logout', function(req, res){
  req.logout()
  res.redirect('/')
})

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword')
})

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(!usuario) return res.render('session/forgotPassword', {info: {message: 'No existe el usuario'}})
    usuario.resetPassword(function(err){
      if(err) return next(err)      
    })
    res.render('session/forgotPasswordMessage')
  })
})

app.get('/resetPassword/:token', function(req, res, next){
  Token.findOne({token: req.params.token}, function(err, token){
    if(!token) return res.status(400).send({type: 'no-verified', msg: 'No existe usuario'})
    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({msg: 'No existe usuario'})
      res.render('session/resetPassword', {errors: {}, usuario: usuario})
    })
  })
})

app.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coinciden los pass'}}, usuario: new Usuario({email: req.body.email})})
    return
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    usuario.password = req.body.password
    usuario.save(function(err){
      if(err) {
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})})
      }else{
        res.redirect('/login')
      }

    })
  })
})

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasRouterApi);
app.use('/api/usuarios', userRApi);
app.use('/api/auth', authAPIRouter)

app.use('/usuarios', usuariosRouter)
app.use('/token', tokenRouter)

app.use('/privacity_publicy', function(req,res){
  res.sendFile('public/privacy_policy.html');
})

app.use('/googlee06a34a4f53852f0', function(req,res){
  res.sendFile('public/googlee06a34a4f53852f0.html');
})

app.use('/google7d1ca08575cb039a', function(req,res){
  res.sendFile('public/google7d1ca08575cb039a.html');
})

app.get('/auth/google',
  passport.authenticate('google', { scope: ['profile', 'email'] })  //Forma de obtener datos de google
)

app.get( '/auth/google/callback', passport.authenticate( 'google', { 
        successRedirect: '/',
        failureRedirect: '/error'
    })
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next()
  }else{
    res.redirect('/login')
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), (err, decoded) => {
    if(err){
      res.json({status: 'error', message: err.message, data: null})
    }else{
      req.body.userId = decoded.id
      console.log('jwt verify: ' + decoded)
      next()
    }
  })
}

module.exports = app;
