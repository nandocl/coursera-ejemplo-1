const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const Usuario = require('../modelos/usuario')
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
    }, function(accessToken, refreshToken, profile, done){
        try {
            use.findOneOrCreateByFacebook(profile, function(err, user){
                if(err) console.log('err' + err);
                return done(err, user);
            });
        } catch(err2){
            console.log(err2);
            return done(err2, null);
        }
    }
));

passport.use(new GoogleStrategy({
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: process.env.HOST + "/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, cb){
        console.log(profile);

        Usuario.finOneOrCreateByGoogle(profile, function(err, user){
            return cb(err, user);
        });
    })
);

passport.use(new localStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, user){
            if(err) return done(err)
            if(!user) return done(null, false, {message: 'No existe usuario'})
            if(!user.validPassword(password)) return done(null, false, {message: 'Error de password'})
            // if(!usuario.verificado) return done(null, false, {message: 'Usuario no verificado'})
            console.log(user)
            return done(null, user)
        })
    }
))

passport.serializeUser(function(user, cb){
    // console.log('serr', user)
    cb(null, user.id)
})

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, user){
        cb(err, user)
    })
})

exports.module