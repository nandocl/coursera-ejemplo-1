const Bicicletas = require('../../modelos/bicicletas.modelos')

exports.read_api_get = (req, res) => {
    Bicicletas.allBicis((err, bicis) => {
        res.status(200).json({
            bicicletas: bicis
        })
    })    
}

exports.add_api_post = (req, res) => {
    let body = req.body;
    let t = Bicicletas.createInstance(body.code, body.color, body.modelo, [body.lat, body.lng])
    Bicicletas.add(t, (err) => {
        res.status(200).send(body)
    })
}

exports.deleteById_api_post = (req, res) => {
    Bicicletas.removeByCode(req.body.code, () => {
        res.send(`borrado id = ${req.body.code}`)
    })
}

exports.update_api_post = (req, res) => {
    let body = req.body;

    Bicicletas.updateByCode(req.params.id, body, (err, bici) => {
        res.status(200).send('updated')
    })
}