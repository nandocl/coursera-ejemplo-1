const Bicicletas = require('../modelos/bicicletas.modelos');

exports.bicicleta_list = (req, res) => {
    Bicicletas.allBicis((err, bicis) => {
        console.log(bicis)
        res.render('bicicletas/index', {allBicicletas: bicis})
    })  
}

exports.crear_bici_get = (req, res) => {
    res.render('bicicletas/crear');
}
exports.crear_bici_pos = (req, res) => {
    let bici = new Bicicletas(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicletas.add(bici);

    res.redirect('/bicicletas');
}

exports.delete_bici_post = (req, res) => {
    let existe = Bicicletas.findById(req.body.id)
    if(existe){
        Bicicletas.deleteById(req.body.id);
        res.redirect('/bicicletas');
    }
}

exports.edit_bici_get = (req, res) => {
    let bici = Bicicletas.findById(req.params.id)
    res.render('bicicletas/edit', {bici});
}
exports.edit_bici_post = (req, res) => {
    let bici = Bicicletas.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color
    bici.modelo = req.body.modelo
    bici.ubicacion = [req.body.lat, req.body.lng]

    res.redirect('/bicicletas');
}