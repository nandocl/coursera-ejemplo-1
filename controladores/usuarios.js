const Usuarios = require('../modelos/usuario')

module.exports = {
    list: function(req, res, next){
        Usuarios.find({}, (err, usuarios) => {
            res.render('usuarios/index', {usuarios: usuarios})
        })
    },
    update_get: function(req, res, next){
        Usuarios.findById(req.params.id, (err, usuario) => {
            res.render('usuarios/update', {errors:{}, usuario: usuario})
        })
    },
    update: function(req, res){
        let update_value = {nombre: req.body.nombre}
        Usuarios.findByIdAndUpdate(req.params.id, update_value, (err, usuario) => {
            if(err){
                console.log(err)
                res.render('usuarios/update', {errors: err.errors, usuario: new Usuarios({nombre: req.body.nombre, email: req.body.email})})
            }else{
                res.redirect('usuarios')
                return
            }
        })
    },
    create_get: function(req, res){
        res.render('usuarios/create', {errors: {}, usuario: new Usuarios()})
    },
    create: function(req, res){
        if(req.body.password != req.body.confirm_pwd){
            res.render('/usuarios/create', {errors: {confirm_password: {message: 'No coinciden los password'}, usuario: new Usuario({name: req.body.nombre, email: req.body.email})}})
            return
        }

        Usuarios.create({name: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsuario){
            if(err){
                res.render('/usuarios/create', {errors: err.errors, usuario: new Usuarios({name: req.body.nombre, email: req.body.email})})
            }else{
                nuevoUsuario.enviar_email_bienvenida()
                res.redirect('/usuarios')
            }
        })
    },
    delete: function(req, res, next){
        Usuarios.findByIdAndDelete(req.body.id, (err) => {
            if(err){
                next(err)
            }else{
                res.redirect('/usuarios')
            }
        })
    }
}