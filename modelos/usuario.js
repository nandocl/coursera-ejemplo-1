const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const Reserva = require('./reserva')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const saltRounds = 10

const Token = require('../modelos/token')
const mailer = require('../mailer/mailer')

const validateEmail = (email) => {
    const re = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/
    return re.test(email)
}

const usuarioSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email válido'],
        match: [/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/]
    },
    password: {
        type: String,
        required: [true, 'El campo password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
})

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next()
})

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'})

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password)
}

usuarioSchema.methods.reserva = function(id, biciId, desde, hasta, cb){
    let reserva = new Reserva({bicicleta: biciId, usuario: id, desde: desde, hasta: hasta})
    reserva.save(cb)
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    token.save(function(err){
        if(err) return console.log((err.message))

        const mailOptions = {
            from: 'no-replay@redbicicletas.com',
            to: email_destination,
            subscribe: 'Verificaion de cuenta',
            text: 'Hola.\n\n' + 'Por favor, para verificar su cuenta haga click en este link_ \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        }

        console.log('enviando', mailOptions)

        mailer.sendMail(mailOptions, (err) => {
            if(err){return console.log(err.message)}
            console.log('Se ha enviado correo de verificacion a' + email_destination + '.')
        })
    })
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    console.log(token)
    token.save(function(err){
        if(err) return cb(err)

        const mailOptions = {
            from: 'no-replay@redbicicletas.com',
            to: email_destination,
            subscribe: 'Reseteo de password ',
            text: 'Hola.\n\n' + 'Por favor, para resetear su password haga click en este link_ \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        }

        console.log('enviando', mailOptions)

        mailer.sendMail(mailOptions, (err) => {
            if(err){return console.log(err.message)}
            console.log('Se ha enviado correo de verificacion a' + email_destination + '.')
        })

    })
}

usuarioSchema.statics.finOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[ 
            {'googleId': condition.id},{'email':condition.emails[0].value} 
        ]},(err, result) => {
            if (result){
                callback(err,result)
            }else{
                console.log('-------- CONDIION ------------');
                console.log(condition);
                let values = {}
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.name = condition.displayName || 'SIN NOMBRE';   //El modelo usuario requiere "name"
                values.verificado = true;
                // values.password = condition._json.etag;   //oAuth ahora no cuenta con este elemento
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('------------ VALUES--------');
                console.log(values);
                self.create(values,(err, result) => {
                    if (err) {console.log(err);}
                    return callback(err, result)
                })     
            }
    })
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[ 
            {'facebookId': condition.id},{'email':condition.emails[0].value} 
        ]},(err, result) => {
            if (result){
                callback(err,result)
            }else{
                console.log('-------- CONDIION ------------');
                console.log(condition);
                let values = {}
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.name = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('------------ VALUES--------');
                console.log(values);
                self.create(values,(err, result) => {
                    if (err) {console.log(err);}
                    return callback(err, result)
                })     
            }
    })
}

module.exports = mongoose.model('Usuario', usuarioSchema)