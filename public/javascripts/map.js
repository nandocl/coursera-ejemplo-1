var map = L.map('mapid').setView([8.245, -73.355], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([8.248, -73.351]).addTo(map);
// L.marker([8.252, -73.362]).addTo(map);

$.ajax({
    dataType: "json",
    url: "/api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(element => {
            L.marker(element.ubicacion).addTo(map);
        });
    }
})