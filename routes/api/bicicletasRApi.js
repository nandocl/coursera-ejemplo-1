const router = require('express').Router();
const biciApiController = require('../../controladores/api/bicicletasAPI')

router.get('/', biciApiController.read_api_get);
router.post('/create', biciApiController.add_api_post);
router.post('/delete', biciApiController.deleteById_api_post);
router.post('/:id/update', biciApiController.update_api_post);

module.exports = router;