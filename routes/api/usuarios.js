const router = require('express').Router();
const usuarioController = require('../../controladores/api/usuarioControllerApi')

router.get('/', usuarioController.usuarios_list)
router.post('/create', usuarioController.usuarios_create)
router.post('/reservar', usuarioController.usuario_reserva)

module.exports = router