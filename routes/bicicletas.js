const router = require('express').Router();

//Controllers
const bicicletaController = require('../controladores/bicicletas.controlador')

router.get('/', bicicletaController.bicicleta_list);

router.get('/crear', bicicletaController.crear_bici_get);
router.post('/crear', bicicletaController.crear_bici_pos);

router.post('/delete', bicicletaController.delete_bici_post);

router.get('/:id/editar', bicicletaController.edit_bici_get);
router.post('/:id/editar', bicicletaController.edit_bici_post);

module.exports = router