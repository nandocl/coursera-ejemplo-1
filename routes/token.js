const router = require('express').Router()
const tokenController = require('./../controladores/token')

router.get('/confirmation/:token', tokenController.confirmationGet)

module.exports = router