const Bicicleta = require('../../modelos/bicicletas.modelos');
const mongoose = require('mongoose')
const request = require('request');
require('../../bin/www');

// beforeEach(() => Bicicleta.allBicicletas = [])

describe('Bicicleta API', () => {

    beforeEach((done) => {
        const mongoUrl = 'mongodb://localhost/test'
        mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true})

        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'Connection error'))
        db.once('open', () => {
            console.log('Connected to test database')
            done()
        })
    })

    afterEach((done) => {
        Bicicleta.deleteMany({}, (error, success) => {
            if(error) console.log(error)
            done()
        })
    })

    describe('GET test /', () => {
        it('Status 200', (done) => {
            request.get('http://localhost:3000/api/bicicletas', (err, res, body) => {
                expect(res.statusCode).toBe(200);
                let result = JSON.parse(body)
                expect(result.bicicletas.length).toBe(0)
                done()
            })
        })
    })

    describe('POST test /create', () => {
        it('Status 200', (done) => {
            let header = {'Content-Type': 'application/json'}
            let url = 'http://localhost:3000/api/bicicletas/create'
            let body = '{"code": 10, "color": "azul", "modelo": "ruta", "lat": 8.248, "lng": -73.351}'

            request.post({headers: header, url: url, body: body}, function(err, res, bodyR) {
                expect(res.statusCode).toBe(200)
                let result = JSON.parse(bodyR)
                expect(result.color).toBe('azul')
                expect(result.lat).toBe(8.248)
                done()
            })
        })
    })

    describe('DELETE test /delete', () => {
        it('Status 200', (done) => {
            let header = {'Content-Type': 'application/json'}
            let url = 'http://localhost:3000/api/bicicletas/create'
            let body = '{"code": 1, "color": "azul", "modelo": "ruta", "lat": 8.248, "lng": -73.351}'

            request.post({headers: header, url: url, body: body}, function(err, res, body) {
                expect(res.statusCode).toBe(200)
                let bici = JSON.parse(body)
                expect(bici.color).toBe('azul')
                expect(bici.lat).toBe(8.248)

                let header2 = {'Content-Type': 'application/json'}
                let url2 = 'http://localhost:3000/api/bicicletas/delete'
                let body2 = '{"code": 1}'
                request.post({headers: header2, url: url2, body: body2}, function(err, res, body) { 

                    Bicicleta.findByCode(1, (err, data) => {
                        expect(data).toBe(null)
                        done()
                    })
                })
            })
        })
    })

    
    describe('UPDATE test /:id/update', () => {
        it('Status 200', (done) => {

            let newE = Bicicleta.createInstance(1, 'gris', 'bmx')

            Bicicleta.add(newE, (err, bici) => {
                Bicicleta.allBicis((err, bicis) => {
                    let header = {'Content-Type': 'application/json'}
                    let url = 'http://localhost:3000/api/bicicletas/1/update'
                    let body = '{"code": 1, "color": "verde", "modelo": "ruta"}'

                    request.post({headers: header, url: url, body: body}, function(err, res, body) {
                        expect(res.statusCode).toBe(200)
                        Bicicleta.allBicis((err, data) => {
                            expect(data[0].color).toBe('verde')
                            expect(data[0].modelo).toBe('ruta')
                            done()
                        })
                    })
                })
            })
        })
    })
    
})