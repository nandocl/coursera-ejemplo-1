const mongoose = require('mongoose')
const Bicicleta = require('../../modelos/bicicletas.modelos');

describe('Testing bicicletas', () => {
    beforeEach((done) => {
        const mongoUrl = 'mongodb://localhost/test'
        mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true})

        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'Connection error'))
        db.once('open', () => {
            console.log('Connected to test database')
            done()
        })
    })

    afterEach((done) => {
        Bicicleta.deleteMany({}, (error, success) => {
            if(error) console.log(error)
            done()
        })
    })

    describe('Bicicleta.createInstancia', () => {
        it('Crear instancia de bicicleta', () => {
            let bici = Bicicleta.createInstance(1, 'verde', 'urbana', [8.251, -73.357])

            expect(bici.code).toBe(1)
            expect(bici.color).toBe('verde')
            expect(bici.modelo).toBe('urbana')
            expect(bici.code).toBe(1)
            expect(bici.ubicacion[0]).toBe(8.251)
            expect(bici.ubicacion[1]).toBe(-73.357)
        })
    })

    describe('Bicicleta.allBicicletas', () => {
        it('Inicia vacío', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0)
                done()
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('Agregar solo una bici', (done) => {
            let biciToAdd = new Bicicleta({code: 1, color: 'gris', modelo: 'mtb'})
            Bicicleta.add(biciToAdd, (err, nb) => {
                if(err) throw console.log('Error')
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toBe(1)
                    expect(bicis[0].color).toBe(biciToAdd.color)
                    done()
                })
            })
        })
    })

    describe('Bicicleta,findByCode', () => {
        it('Devuelve bici con codigo 1', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0)
                let bici1 = new Bicicleta({code: 1, color: 'gris', modelo: 'mtb'})
                Bicicleta.add(bici1, (err, nv1) => {
                    if(err) throw console.log(err)

                    let bici2 = new Bicicleta({code: 2, color: 'rojo', modelo: 'montaña'})
                    Bicicleta.add(bici2, (err2, nv2) => {
                        if(err2) throw console.log(err2)
                        Bicicleta.findByCode(1, (err, bici) => {
                            expect(bici.color).toBe('gris')
                            done()
                        })
                    })

                })
            })
        })
    })

    describe('Bicicleta.removeById', () => {
        it('Verifica existencia y borrado de elemento', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0)
                let nBici = new Bicicleta({code: 10, color: 'naranja', modelo: 'semi carrera', ubicaion: [8.258, -73.365]})
                Bicicleta.add(nBici, (err, bici) => {
                    Bicicleta.allBicis((err, bicis) => {
                        expect(bicis.length).toBe(1)
                        Bicicleta.removeByCode(10, (err, bici) => {
                            Bicicleta.allBicis((err, bicis) => {
                                expect(bicis.length).toBe(0)
                                done()
                            })
                        })
                    })
                })
            })
        })
    })


})